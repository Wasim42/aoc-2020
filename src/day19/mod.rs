mod input;

use std::collections::HashMap;
use regex::Regex;

fn build_regex_for_zero(rules: &str, part_two: bool) -> String {
    let mut builder: HashMap<usize, Vec<Vec<usize>>> = HashMap::new();

    let mut letters: HashMap<usize, char> = HashMap::new();

    for line in rules.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        if line.chars().next().unwrap().is_digit(10) {
            // Handling the builder phase
            let mut values = line.split(": ");
            let index = values.next().unwrap().parse::<usize>().unwrap();

            // Make sure there's room in the vector
            if builder.capacity() < index {
                builder.reserve(index - builder.capacity());
            }

            // We are now parsing the entries of the index
            let entries = values.next().unwrap();
            if entries.starts_with('"') {
                // This is a single character entry
                letters.insert(index, entries.chars().nth(1).unwrap());
                continue;
            }

            let mut entry: Vec<Vec<usize>> = Vec::new();
            for entries in entries.split(" | ") {
                let numbers = entries.split(' ')
                    .map(|x| x.parse::<usize>().unwrap())
                    .collect::<Vec<_>>();
                entry.push(numbers);
            }
            builder.insert(index, entry);
        }
    }


    regex_builder(&builder, &letters, 0, part_two)
}

fn regex_builder(
    builder: &HashMap<usize, Vec<Vec<usize>>>,
    letters: &HashMap<usize, char>,
    position: usize,
    part_two: bool,
    ) -> String {

    // Recursively return a string representing the regex value here
    // Each entry is a series of alternatives for this position, unless it's a
    // reference to an 
    if part_two {
        // We have to handle these very special rules

        // 8: 42 | 42 8
        // This is simply (42)+
        if position == 8 {
            return format!("({})+", regex_builder(builder, letters, 42, part_two));
        }
        // 11: 42 31 | 42 11 31
        // This is a lot more complex as it could be:
        // (42,31|42,42,31,31|42,42,42,31,31,31|…)
        // The easiest way is to transform it into:
        // (42{n},31{n}|42{n},31{n}…) for a number of n's.  It's impossible to
        // know how many will suffice, but 5 sounds like a good limit.
        let limit = 5;
        if position == 11 {
            let number_42 = regex_builder(builder, letters, 42, part_two);
            let number_31 = regex_builder(builder, letters, 31, part_two);
            let mut section = "(".to_string();
            for count in 1..(limit+1) {
                section += &format!("{}{{{}}}{}{{{}}}",
                    number_42, count, number_31, count);
                if count < limit {
                    section += "|";
                }
            }
            section += ")";
            return section;
        }
    }
    if let Some(entries) = builder.get(&position) {
        let mut section = String::new();
        if entries.len() > 1 {
            section += "(";
        }
        for (index, entry) in entries.iter().enumerate() {
            // Part 2 replaces a couple of rules
            for index in entry {
                section += &regex_builder(builder, letters, *index, part_two);
            }
            if index < (entries.len()-1) {
                section += "|";
            }
        }
        if entries.len() > 1 {
            section += ")";
        }
        section
    } else {
        // Yipee - it's a final letter
        return letters.get(&position).unwrap().to_string()
    }
}

fn count_matches(rules: &str, part_two: bool) -> i32 {
    let re = build_regex_for_zero(rules, part_two);
    // Note that we need to set the regex to be more restrictive
    let re = "^".to_string() + &re + "$";
    let re = Regex::new(&re).unwrap();

    let mut counter = 0;

    // Now extract the strings
    for line in rules.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        if line.chars().next().unwrap().is_digit(10) {
            // One of the builders - ignore
            continue;
        }

        // line is a string to match
        if re.is_match(line) {
            counter += 1;
        }
    }

    counter
}
pub fn part1() -> String {
    format!("{}", count_matches(input::INPUT, false))
}

pub fn part2() -> String {
    format!("{}", count_matches(input::INPUT, true))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_regexbuilder() {
        let question = r#"
            0: 1 2
            1: "a"
            2: 1 3 | 3 1
            3: "b"
            "#;
        assert_eq!("a(ab|ba)", build_regex_for_zero(question, false));
    }

    #[test]
    fn test_part1() {
        let testdata = r#"
            0: 4 1 5
            1: 2 3 | 3 2
            2: 4 4 | 5 5
            3: 4 5 | 5 4
            4: "a"
            5: "b"

            ababbb
            bababa
            abbbab
            aaabbb
            aaaabbb
            "#;
        assert_eq!(2, count_matches(testdata, false));
    }
    #[test]
    fn test_part2() {
        let testdata = r#"
            42: 9 14 | 10 1
            9: 14 27 | 1 26
            10: 23 14 | 28 1
            1: "a"
            11: 42 31
            5: 1 14 | 15 1
            19: 14 1 | 14 14
            12: 24 14 | 19 1
            16: 15 1 | 14 14
            31: 14 17 | 1 13
            6: 14 14 | 1 14
            2: 1 24 | 14 4
            0: 8 11
            13: 14 3 | 1 12
            15: 1 | 14
            17: 14 2 | 1 7
            23: 25 1 | 22 14
            28: 16 1
            4: 1 1
            20: 14 14 | 1 15
            3: 5 14 | 16 1
            27: 1 6 | 14 18
            14: "b"
            21: 14 1 | 1 14
            25: 1 1 | 1 14
            22: 14 14
            8: 42
            26: 14 22 | 1 20
            18: 15 15
            7: 14 5 | 1 21
            24: 14 1

            abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
            bbabbbbaabaabba
            babbbbaabbbbbabbbbbbaabaaabaaa
            aaabbbbbbaaaabaababaabababbabaaabbababababaaa
            bbbbbbbaaaabbbbaaabbabaaa
            bbbababbbbaaaaaaaabbababaaababaabab
            ababaaaaaabaaab
            ababaaaaabbbaba
            baabbaaaabbaaaababbaababb
            abbbbabbbbaaaababbbbbbaaaababb
            aaaaabbaabaaaaababaa
            aaaabbaaaabbaaa
            aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
            babaaabbbaaabaababbaabababaaab
            aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba
            "#;
        assert_eq!(3, count_matches(testdata, false));
        assert_eq!(12, count_matches(testdata, true));
    }
}
