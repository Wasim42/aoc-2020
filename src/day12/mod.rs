mod input;

enum Orientation {
    North,
    East,
    South,
    West,
}

use Orientation::*;

impl Orientation {
    fn turn_right_by(self, degrees: i32) -> Orientation {
        let mut dir = self;
        for _ in 0..degrees/90 {
            dir = dir.turn_right();
        }
        dir
    }
    fn turn_right(self) -> Orientation {
        match self {
            North => East,
            East => South,
            South => West,
            West => North,
        }
    }
    fn turn_left_by(self, degrees: i32) -> Orientation {
        let mut dir = self;
        for _ in 0..degrees/90 {
            dir = dir.turn_left();
        }
        dir
    }
    fn turn_left(self) -> Orientation {
        match self {
            North => West,
            East => North,
            South => East,
            West => South,
        }
    }
}

fn parse_input(instructions: &str) -> Vec<(char, i32)> {

    let mut moves = Vec::new();

    for line in instructions.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        let dir = line.chars().next().unwrap();
        let amount = line[1..].parse::<i32>().unwrap();
        moves.push((dir, amount));
    }
    moves
}

fn motion(x: i32, y: i32, dir: &Orientation, amount: i32) -> (i32, i32) {

    let mut x = x;
    let mut y = y;

    match dir {
        North => y += amount,
        East => x += amount,
        South => y -= amount,
        West => x -= amount,
    }
    (x, y)
}

fn do_part1(instructions: &str) -> i32 {
    let instructions = parse_input(instructions);

    let mut x = 0;
    let mut y = 0;
    let mut dir = East;
    let mut xy;

    for (turn, amount) in instructions {
        match turn {
            'N' => { xy = motion(x, y, &North, amount); x = xy.0; y = xy.1 },
            'E' => { xy = motion(x, y, &East, amount); x = xy.0; y = xy.1 },
            'S' => { xy = motion(x, y, &South, amount); x = xy.0; y = xy.1 },
            'W' => { xy = motion(x, y, &West, amount); x = xy.0; y = xy.1 },
            'R' => dir = dir.turn_right_by(amount),
            'L' => dir = dir.turn_left_by(amount),
            'F' => { xy = motion(x, y, &dir, amount); x = xy.0; y = xy.1 },
            _ => panic!("Unexpected turn {}", turn),
        }
    }
    x.abs() + y.abs()
}

pub fn part1() -> String {
    format!("{}", do_part1(input::INPUT))
}

fn rotate(fixed: &(i32, i32), point: (i32, i32), amount: i32) -> (i32, i32) {
    // Rotate a point around a fixed point by an amount of degrees
    let mut clockwise = true;
    if amount < 0 {
        clockwise = false;
    }
    let mut amount = amount.abs();


    let mut rel_x = point.0 - fixed.0;
    let mut rel_y = point.1 - fixed.1;

    while amount > 0 {
        amount -= 90;
        if clockwise {
            let new_rel_x = rel_y;
            let new_rel_y = -rel_x;
            rel_x = new_rel_x;
            rel_y = new_rel_y;
        } else {
            let new_rel_y = rel_x;
            let new_rel_x = -rel_y;
            rel_x = new_rel_x;
            rel_y = new_rel_y;
        }
    }

    (fixed.0 + rel_x, fixed.1 + rel_y)
}

fn move_by(start: (i32, i32), waypoint: &(i32, i32), amount: i32) -> (i32, i32) {
    let rel_x = waypoint.0 - start.0;
    let rel_y = waypoint.1 - start.1;

    (start.0 + amount*rel_x, start.1 + amount*rel_y)
}

fn do_part2(instructions: &str) -> i32 {
    let instructions = parse_input(instructions);

    let mut waypoint_x = 10;
    let mut waypoint_y = 1;
    let mut wxy;

    let mut x = 0;
    let mut y = 0;
    let mut xy;

    for (turn, amount) in instructions {
        match turn {
            'N' => {
                wxy = motion(waypoint_x, waypoint_y, &North, amount);
                waypoint_x = wxy.0; waypoint_y = wxy.1 },
            'E' => {
                wxy = motion(waypoint_x, waypoint_y, &East, amount);
                waypoint_x = wxy.0; waypoint_y = wxy.1 },
            'S' => {
                wxy = motion(waypoint_x, waypoint_y, &South, amount);
                waypoint_x = wxy.0; waypoint_y = wxy.1 },
            'W' => {
                wxy = motion(waypoint_x, waypoint_y, &West, amount);
                waypoint_x = wxy.0; waypoint_y = wxy.1 },
            'R' => {
                wxy = rotate(&(x, y), (waypoint_x, waypoint_y), amount);
                waypoint_x = wxy.0; waypoint_y = wxy.1 },
            'L' => {
                wxy = rotate(&(x, y), (waypoint_x, waypoint_y), -amount);
                waypoint_x = wxy.0; waypoint_y = wxy.1 },
            'F' => {
                let rel_x = waypoint_x - x;
                let rel_y = waypoint_y - y;
                xy = move_by((x, y), &(waypoint_x, waypoint_y), amount);
                x = xy.0; y = xy.1;
                waypoint_x = x + rel_x; waypoint_y = y + rel_y; },
            _ => panic!("Unexpected turn {}", turn),
        }
    }
    x.abs() + y.abs()
}

pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tests() {
        let testdata1 = r#"
            F10
            N3
            F7
            R90
            F11
            "#;
        assert_eq!(25, do_part1(testdata1));
        assert_eq!(286, do_part2(testdata1));
    }
}
