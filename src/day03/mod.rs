mod input;
use super::structs::Location;


use std::collections::{HashSet};

fn parse_treeslope(treeslope: &str) -> (i32, i32, HashSet<Location>) {
    /* We only store the locations of the trees - there's no point storing
     * empty space!
     * We return the width and height as otherwise we have no idea about the
     * limits of the slope we just parsed.
     */
    let mut trees = HashSet::new();

    let mut width = 0;
    let mut height = 0;
    for line in treeslope.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        if line.len() > width {
            width = line.len();
        }
        for (x, pos) in line.chars().enumerate() {
            if pos == '#' {
                trees.insert(Location{x: x as i32, y: height});
            }
        }
        height += 1;
    }

    (width as i32, height, trees)
}

fn do_hitcount(trees: &str, right: i32, down: i32) -> i32 {
    let (width, height, trees) = parse_treeslope(trees);
    let mut x = 0;
    let mut y = 0;

    let mut hits = 0;

    while y < height {
        if trees.contains(&Location{x, y}) {
            hits += 1;
        }
        x = (x+right)%width;
        y += down;
    }
    hits
}

fn do_part1(trees: &str) -> i32 {
    do_hitcount(trees, 3, 1)
}

fn do_part2(trees: &str) -> i32 {
    do_hitcount(trees, 1, 1) *
        do_hitcount(trees, 3, 1) *
        do_hitcount(trees, 5, 1) *
        do_hitcount(trees, 7, 1) *
        do_hitcount(trees, 1, 2)
}

pub fn part1() -> String {
    format!("{}", do_part1(input::INPUT))
}

pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT))
}


#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = r#"
    ..##.......
    #...#...#..
    .#....#..#.
    ..#.#...#.#
    .#...##..#.
    ..#.##.....
    .#.#.#....#
    .#........#
    #.##...#...
    #...##....#
    .#..#...#.#"#;

    #[test]
    fn test_d03_part1() {
        assert_eq!(7, do_part1(TEST_INPUT));
    }

    #[test]
    fn test_d03_part2() {
        assert_eq!(336, do_part2(TEST_INPUT));
    }
}
