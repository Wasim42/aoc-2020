use std::collections::{HashSet, HashMap};

mod input;


pub fn do_part1(answers: &str) -> i32 {
    let mut total = 0;

    let mut counter = HashSet::new();

    for line in answers.lines() {
        let line = line.trim();
        if line.is_empty() {
            // Blank line - end of an era
            total += counter.len();
            counter = HashSet::new();
            continue;
        }

        for a in line.chars() {
            counter.insert(a);
        }

    }
    total as i32
}

pub fn part1() -> String {
    format!("{}", do_part1(input::INPUT))
}

pub fn do_part2(answers: &str) -> i32 {
    let mut total = 0;

    let mut group_counter: HashMap<char, i32> = HashMap::new();
    let mut group_len = 0;

    for line in answers.lines() {
        let line = line.trim();
        if line.is_empty() {
            // Blank line - end of an era
            // Total number is where the total number of yes's is the same as
            // the group_len
            let mut this_total = 0;
            for (_, count) in group_counter {
                if count == group_len {
                    this_total += 1;
                }
            }
            total += this_total;
            group_counter = HashMap::new();
            group_len = 0;
            continue;
        }

        group_len += 1;

        // We need to ensure that we don't have duplicate entries on a line,
        // otherwise this simple algorithm will fall over!
        let v: Vec<char> = line.chars().collect();
        let mut y = v.clone();
        y.dedup();

        assert_eq!(y.len(), v.len());
        
        for a in v {
            let answer_count = group_counter.entry(a).or_insert(0);
            *answer_count += 1;
        }

    }
    total as i32
}


pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_d06() {
        let testdata = r#"
            abc

            a
            b
            c

            ab
            ac

            a
            a
            a
            a

            b
            "#;
        assert_eq!(11, do_part1(testdata));
        assert_eq!(6, do_part2(testdata));
    }
}
