use std::collections::{HashSet, HashMap};

mod input;


fn do_part1(bag_string: &str) -> usize {
    let desired = "shiny gold";

    // We are going to store things in a HashMap of a Set of strings.
    // We are more interested in cataloging the holders of a specific bag rather
    // than the contents of a specific type of bag.  Therefore when we've
    // finished, bag_tree will hold:
    // light red bags contain 1 bright white bag, 2 muted yellow bags.
    // as:
    // {"bright white": [light red], "muted yellow": [light red]}
    //
    // We won't bother with the number as we don't need it yet (might not need
    // it for part 2, so it's not trying to speculate).

    let mut bag_tree = HashMap::new();

    for line in bag_string.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        // light red bags contain 1 bright white bag, 2 muted yellow bags.
        // If we split on " bags contain " we will have the bag type, and its
        // contets.
        let mut bag_details = line.split(" bags contain ");

        let bag_type = bag_details.next().unwrap();

        for bag in bag_details.next().unwrap().split(", ") {
            // bag will be something like:
            // "2 muted yellow bags." - we only want the middle two words.
            // Split by space, and we can pick up the two middle ones.
            let mut bag_desc = bag.split(' ');
            let bag_count = bag_desc.next().unwrap();
            if bag_count == "no" {
                // No other bags - we're done!
                break;
            }
            let bag_adj = bag_desc.next().unwrap();
            let bag_col = bag_desc.next().unwrap();
            let bag_content = format!("{} {}", bag_adj, bag_col);
            let bag_contents = bag_tree.entry(bag_content)
                 .or_insert_with(HashSet::new);
            bag_contents.insert(bag_type);
        }
    }

    let mut found_bags = HashSet::new();

    let mut finders: Vec<_> = bag_tree.get(desired).unwrap().iter().collect();
    while !finders.is_empty() {
        let bag = finders.pop().unwrap();

        if let Some(new_bags) = bag_tree.get(&bag.to_string()) {
            for new_bag in new_bags {
                finders.push(new_bag);
            }
        }
        found_bags.insert(bag);
    }

    found_bags.len()
}

pub fn part1() -> String {
    format!("{}", do_part1(input::INPUT))
}

fn do_part2(bag_string: &str) -> i32 {
    let desired = "shiny gold";

    // We are going to store things in a HashMap of a Set of strings.
    // We need to store the number of bags along with the colour.  Therefore
    // when we've finished, bag_tree will hold:
    // light red bags contain 1 bright white bag, 2 muted yellow bags.
    // as:
    // {"light red": [(1, "bright white"), (2, "muted yellow")]}

    let mut bag_tree = HashMap::new();

    for line in bag_string.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        // light red bags contain 1 bright white bag, 2 muted yellow bags.
        // If we split on " bags contain " we will have the bag type, and its
        // contets.
        let mut bag_details = line.split(" bags contain ");

        let bag_type = bag_details.next().unwrap();

        let mut bag_contents = Vec::new();

        for bag in bag_details.next().unwrap().split(", ") {
            // bag will be something like:
            // "2 muted yellow bags." - we only want the middle two words.
            // Split by space, and we can pick up the two middle ones.
            let mut bag_desc = bag.split(' ');
            let bag_count = bag_desc.next().unwrap();
            if bag_count == "no" {
                // No other bags - we're done!
                break;
            }
            let bag_count = bag_count.parse::<i32>().unwrap();
            let bag_adj = bag_desc.next().unwrap();
            let bag_col = bag_desc.next().unwrap();
            let bag_content = format!("{} {}", bag_adj, bag_col);
            bag_contents.push((bag_count, bag_content));
        }

        bag_tree.insert(bag_type, bag_contents);
    }

    // Now that we've got our bag_tree, we need to add up all the contents of
    // our shiny gold bag.

    // There are two ways of solving this puzzle:
    // 1. We could be naive and, for example if it says (6, "dark blue") we
    //    could look at the contents of "dark blue" and add them 6 times to the
    //    end of bags.
    // 2. We could be a bit smarter and for "dark blue" count the number of bags
    //    contained therein in a recursive manner.
    //
    // I'll go for the recursive approach!

    // Remember to not count the shiny gold bag!
    count_bags(&bag_tree, desired) - 1
}

fn count_bags(bag_tree: &HashMap<&str, Vec<(i32, String)>>, desired: &str) -> i32 {
    let mut total = 1;
    let bags = bag_tree.get(desired).unwrap();

    for (count, bag) in bags {
        total += count * count_bags(&bag_tree, bag);
    }
    total
}

pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_d07() {
        let testdata1 = r#"
            light red bags contain 1 bright white bag, 2 muted yellow bags.
            dark orange bags contain 3 bright white bags, 4 muted yellow bags.
            bright white bags contain 1 shiny gold bag.
            muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
            shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
            dark olive bags contain 3 faded blue bags, 4 dotted black bags.
            vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
            faded blue bags contain no other bags.
            dotted black bags contain no other bags.
            "#;
        assert_eq!(4, do_part1(testdata1));
        assert_eq!(32, do_part2(testdata1));
        let testdata2 = r#"
            shiny gold bags contain 2 dark red bags.
            dark red bags contain 2 dark orange bags.
            dark orange bags contain 2 dark yellow bags.
            dark yellow bags contain 2 dark green bags.
            dark green bags contain 2 dark blue bags.
            dark blue bags contain 2 dark violet bags.
            dark violet bags contain no other bags.
            "#;
        assert_eq!(126, do_part2(testdata2));
    }
}
