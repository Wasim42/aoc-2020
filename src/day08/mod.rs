use std::collections::HashSet;
use std::convert::From;

mod input;

#[derive(Debug, Clone, PartialEq)]
enum Opcode {
    Nop,
    Acc,
    Jmp,
}

impl From<&str> for Opcode {
    fn from(item: &str) -> Self {
        match item {
            "nop" => Opcode::Nop,
            "acc" => Opcode::Acc,
            "jmp" => Opcode::Jmp,
            _ => panic!("Invalid Opcode {}", item),
        }
    }
}

impl Opcode {
    pub fn twist(&self) -> Opcode {
        match self {
            Opcode::Nop => Opcode::Jmp,
            Opcode::Acc => Opcode::Acc,
            Opcode::Jmp => Opcode::Nop,
        }
    }
}

fn parse_code(instructions: &str) -> Vec<(Opcode, i32)> {
    let mut code = Vec::new();

    for line in instructions.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        let mut opparam = line.split(' ');
        let op = Opcode::from(opparam.next().unwrap());
        let param = opparam.next().unwrap();
        code.push((op, param.parse::<i32>().unwrap()));
    }

    code
}

fn do_part1(instructions: &str) -> i32 {

    let code = parse_code(instructions);

    let twist = code.len();
    exec(&code, twist).0
}

fn exec(code: &[(Opcode, i32)], twist: usize) -> (i32, bool) {
    // The return parameters are:
    // .0 (i32) Accumulator
    // .1 (bool) whether we terminated without an infinite loop

    let mut acc = 0;
    let mut pc = 0;

    let mut infinite = false;
    let mut visited = HashSet::new();

    while pc < code.len() {
        if visited.contains(&pc) {
            infinite = true;
            break;
        }
        visited.insert(pc);

        let (mut op, param) = code[pc].clone();

        if pc == twist {
            op = op.twist();
        }
        match op {
            Opcode::Nop => pc += 1,
            Opcode::Acc => { acc += param; pc += 1;},
            Opcode::Jmp => { pc = (pc as i32 + param) as usize; }
        }
    }
    (acc, infinite)
}

pub fn part1() -> String {
    format!("{}", do_part1(input::INPUT))
}

fn do_part2(instructions: &str) -> i32 {
    let code = parse_code(instructions);

    let mut twist = 0;
    let mut answer = 0;
    let mut fail: bool;
    let limit = code.len();
    while twist < limit {
        // Increment twist to the next nop/jmp
        while twist < limit {
            let op = &code[twist].0;
            if op == &Opcode::Acc {
                twist+= 1;
            } else {
                break;
            }
        }
        let ret = exec(&code, twist);
        answer = ret.0;
        fail = ret.1;
        if !fail {
            break;
        }
        twist += 1;
    }
    answer
}

pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_d08() {
        let testdata1 = r#"
            nop +0
            acc +1
            jmp +4
            acc +3
            jmp -3
            acc -99
            acc +1
            jmp -4
            acc +6
            "#;
        assert_eq!(5, do_part1(testdata1));
        assert_eq!(8, do_part2(testdata1));
    }
}
