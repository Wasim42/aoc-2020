mod input;

use super::structs::Location;
use std::collections::HashMap;


fn parse_cells(cells: &str) -> (HashMap<Location, bool>, Location) {
    let mut cell_map = HashMap::new();
    let mut pos = Location{x: 0, y: -1};

    let mut max_x = 0;
    let mut max_y = 0;
    for line in cells.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        pos.x = -1;
        pos.y += 1;
        max_y += 1;

        for c in line.chars() {
            pos.x += 1;
            if c == 'L' {
                // There is an empty seat here
                cell_map.insert(pos.clone(), false);
            }
        }
        if pos.x > max_x {
            max_x = pos.x;
        }
    }
    max_y = pos.y;

    (cell_map, Location{x: max_x, y: max_y})
}

fn apply_changelog(cell_map: &mut HashMap<Location, bool>, changelog: Vec<Location>) {
    for change in changelog {
        let seat = cell_map.entry(change).or_insert(false);
        *seat = !*seat;
    }

}

#[allow(dead_code)]
fn print_map(cell_map: &HashMap<Location, bool>, end_corner: &Location) {

    println!();
    for y in 0..end_corner.y+1 {
        for x in 0..end_corner.x+1 {
            if let Some(seat) = cell_map.get(&Location{x, y}) {
                if *seat {
                    print!("#", );
                } else {
                    print!("L", );
                }
            } else {
                print!(".", );
            }
        }
        println!();
    }
}

fn get_equilibrium(cells: &str, occupancy_limit: i32,
    adj_fn: fn (&HashMap<Location, bool>, &Location, &Location, i32) -> bool) -> i32 {

    let (mut cell_map, end_corner) = parse_cells(cells);

    loop {
        let changelog = get_changelog(&cell_map, &end_corner, occupancy_limit, adj_fn);
        //print_map(&cell_map, &end_corner);
        if changelog.is_empty() {
            break;
        }

        apply_changelog(&mut cell_map, changelog);
    }

    // Now count how many seats are occupied
    let mut occupied = 0;
    for (_, seat) in cell_map {
        if seat {
            occupied += 1;
        }
    }

    occupied
}

fn is_adj(cell_map: &HashMap<Location, bool>, from: &Location,
    _end_corner: &Location, limit: i32) -> bool {
    // Returns true if greater than the limit of neighbours are occupied
    // Adjacent to L:
    // # # #
    // # L #
    // # # #
    let mut occupants = 0;

    let dxdy = [
        (-1, -1), (0, -1), (1, -1),
        (-1,  0),          (1,  0),
        (-1,  1), (0,  1), (1,  1),
    ];
    for (dx, dy) in dxdy.iter() {
        if let Some(neighbour) = cell_map
            .get(&Location{x: from.x + dx, y: from.y + dy}) {
                if *neighbour {
                    occupants += 1;
                    if occupants > limit {
                        return true;
                    }
                }
            }
    }
    false
}

fn is_visible(cell_map: &HashMap<Location, bool>,
    from: &Location, end_corner: &Location, limit: i32) -> bool {
    // Returns true if greater than the limit of visible neighbours are occupied
    // Visible to L:
    // X   X   X
    //   X X X
    // X X L X X
    //   X X X
    // X   X   X

    let mut occupants = 0;

    let mut dxdy = vec![
        (-1, -1), (0, -1), (1, -1),
        (-1,  0),          (1,  0),
        (-1,  1), (0,  1), (1,  1),
    ];
    let mut multiplier = 1;
    while !dxdy.is_empty() {
        for (dx, dy) in dxdy.clone() {
            let x = from.x + dx*multiplier;
            let y = from.y + dy*multiplier;
            if x < 0 || x > end_corner.x ||
                y < 0 || y > end_corner.y {
                    // We've passed the limit!
                    let index = dxdy.iter().position(|&r| r == (dx, dy)).unwrap();
                    dxdy.remove(index);
            }
            if let Some(neighbour) = cell_map
                .get(&Location{x, y}) {
                    // We have found a seat - remove it from our search
                    let index = dxdy.iter().position(|&r| r == (dx, dy)).unwrap();
                    dxdy.remove(index);
                    if *neighbour {
                        occupants += 1;
                        if occupants > limit {
                            return true;
                        }
                    }
                }
        }
        multiplier += 1;
    }
    false
}


fn get_changelog(cell_map: &HashMap<Location, bool>,
    end_corner: &Location,
    occupancy_limit: i32,
    adj_fn: fn (&HashMap<Location, bool>, &Location, &Location, i32) -> bool) -> Vec<Location> {
    let mut changelog = Vec::new();

    let mut pos = Location{x: 0, y: 0};

    for y in 0..end_corner.y+1 {
        for x in 0..end_corner.x+1 {
            pos.x = x;
            pos.y = y;
            if let Some(seat) = cell_map.get(&pos) {
                if *seat {
                    // If a seat is occupied and four or more seats adjacent
                    // to it are also occupied, the seat becomes empty.
                    if adj_fn(&cell_map, &pos, &end_corner, occupancy_limit) {
                        changelog.push(Location{x, y});
                    }
                } else {
                    // If a seat is empty and there are no occupied seats
                    // adjacent to it, the seat becomes occupied
                    if !adj_fn(&cell_map, &pos, &end_corner, 0) {
                        changelog.push(Location{x, y});
                    }
                }
            }
        }
    }
    changelog
}

pub fn part1() -> String {
    format!("{}", get_equilibrium(input::INPUT, 3, is_adj))
}

pub fn part2() -> String {
    format!("{}", get_equilibrium(input::INPUT, 4, is_visible))
}

#[cfg(test)]
mod tests {
    use super::*;

    const TESTDATA1: &str = r#"
        L.LL.LL.LL
        LLLLLLL.LL
        L.L.L..L..
        LLLL.LL.LL
        L.LL.LL.LL
        L.LLLLL.LL
        ..L.L.....
        LLLLLLLLLL
        L.LLLLLL.L
        L.LLLLL.LL
        "#;
    #[test]
    fn test_part1() {
        assert_eq!(37, get_equilibrium(TESTDATA1, 3, is_adj));
    }
    #[test]
    fn test_part2() {
        assert_eq!(26, get_equilibrium(TESTDATA1, 4, is_visible));
    }
}
