mod input;

use std::collections::HashSet;

fn debinary(binary: &str, one: char, zero: char) -> i32 {

    let binary: String = binary
        .chars()
        .map(|x| match x {
            x if x == one => '1',
            x if x == zero => '0',
            _ => x,
        })
    .collect();
    i32::from_str_radix(&binary, 2).unwrap()
}

fn parse_seat(seat: &str) -> (i32, i32) {

    let row = &seat[..7];
    let column = &seat[7..];
    (debinary(row, 'B', 'F'), debinary(column, 'R', 'L'))
}

pub fn part1() -> String {
    let mut answer = 0;
    for seat in input::INPUT.lines() {
        if seat.is_empty() {
            continue;
        }
        let (row, column) = parse_seat(seat);
        if row * 8 + column > answer {
            answer = row * 8 + column;
        }
    }
    format!("{}", answer)
}

pub fn part2() -> String {
    // Which seats are taken?
    let mut taken_seats = HashSet::new();
    for seat in input::INPUT.lines() {
        if seat.is_empty() {
            continue;
        }
        let (row, column) = parse_seat(seat);
        taken_seats.insert(row*8 + column);
    }
    // Scanning through the possible seats, find the missing seat
    for row in 0..128 {
        for column in 0..8 {
            let seatid = row * 8 + column;
            if !taken_seats.contains(&seatid) {
                // A not-taken seat - check the adjacency
                if taken_seats.contains(&(seatid-1))
                    && taken_seats.contains(&(seatid+1)) {
                        return format!("{}", seatid);
                }
            }
        }
    }
    "Not Found".to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_d05_seats() {
        assert_eq!((44, 5), parse_seat("FBFBBFFRLR"));
        assert_eq!((70, 7), parse_seat("BFFFBBFRRR"));
        assert_eq!((14, 7), parse_seat("FFFBBBFRRR"));
        assert_eq!((102, 4), parse_seat("BBFFBBFRLL"));
    }
}
