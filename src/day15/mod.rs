const INPUT: [usize;7] = [0,1,5,10,3,12,19];

use std::collections::HashMap;

fn do_part1(start: &[usize], stopat: usize) -> usize {

    let mut number = start[0];

    // 0, 3, 6
    // Turn 1: The 1st number spoken is a starting number, 0.
    // Turn 2: The 2nd number spoken is a starting number, 3.
    // Turn 3: The 3rd number spoken is a starting number, 6.
    // Turn 4: Now, consider the last number spoken, 6. Since that was the first
    // time the number had been spoken, the 4th number spoken is 0.
    // Turn 5: Next, again consider the last number spoken, 0. Since it had been
    // spoken before, the next number to speak is the difference between the
    // turn number when it was last spoken (the previous turn, 4) and the turn
    // number of the time it was most recently spoken before then (turn 1).
    // Thus, the 5th number spoken is 4 - 1, 3.
    // Turn 6: The last number spoken, 3 had also been spoken before, most
    // recently on turns 5 and 2. So, the 6th number spoken is 5 - 2, 3.
    // Turn 7: Since 3 was just spoken twice in a row, and the last two turns
    // are 1 turn apart, the 7th number spoken is 1.
    // Turn 8: Since 1 is new, the 8th number spoken is 0.

    // We first initalise our memory
    // { number: last_seen }
    let mut memory = HashMap::new();
    let mut turn = 0;

    for i in start {
        turn += 1;
        memory.insert(*i, turn);
        number = *i;
    }

    // Now to play the game
    let mut last_time: usize;
    while turn < stopat {
        if let Some(previous_time) = memory.get(&number) {
            last_time = *previous_time;
        } else {
            last_time = turn;
        }
        memory.insert(number, turn);
        number = turn - last_time;
        turn += 1;
    }

    number
}

pub fn part1() -> String {
    format!("{}", do_part1(&INPUT, 2020))
}

pub fn part2() -> String {
    format!("{}", do_part1(&INPUT, 30000000))
}

#[cfg(test)]
mod tests {
    use super::*;
    use rayon::prelude::*;
    

    // This day takes so long to run that we need to parallelise the test
    #[test]
    fn test_day() {
        let testdata = vec![
            ([0,3,6], 2020, 436),
            ([1,3,2], 2020, 1),
            ([2,1,3], 2020, 10),
            ([1,2,3], 2020, 27),
            ([2,3,1], 2020, 78),
            ([3,2,1], 2020, 438),
            ([3,1,2], 2020, 1836),
            ([0,3,6], 30000000, 175594),
            ([1,3,2], 30000000, 2578),
            ([2,1,3], 30000000, 3544142),
            ([1,2,3], 30000000, 261214),
            ([2,3,1], 30000000, 6895259),
            ([3,2,1], 30000000, 18),
            ([3,1,2], 30000000, 362),
        ];
        testdata.into_par_iter().for_each(|(question, rounds, answer)|
            assert_eq!(answer, do_part1(&question, rounds)));
    }
}
