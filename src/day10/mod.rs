mod input;

fn to_numbers(numbers: &str) -> Vec<i32> {
    let mut nums = vec![0];
    let mut highest = 0;
    for line in numbers.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        let number = line.parse::<i32>().unwrap();
        if number > highest {
            highest = number;
        }
        nums.push(number);
    }
    nums.push(highest + 3);
    nums.sort();
    nums
}

fn get_jolt_diffs(adapters: &str) -> (i32, i32) {

    let adapters = to_numbers(adapters);
    let mut diff_3 = 0;
    let mut diff_1 = 0;
    let mut prev = 0;
    for i in adapters {
        if i - prev == 1 {
            diff_1 += 1;
        }
        if i - prev == 3 {
            diff_3 += 1;
        }
        prev = i;
    }

    (diff_1, diff_3)
}

pub fn part1() -> String {
    let answer = get_jolt_diffs(input::INPUT);
    format!("{}", answer.0 * answer.1)
}

fn do_part2(adapters: &str) -> usize {
    let adapters = to_numbers(adapters);
    // The formula:
    // Find all possible diff_3 and diff_2 in the list of adapters, and see
    // which omes can fit into the gap.  So consider:
    // 1, 4, 5, 6, 7, 10
    // 1, 4 is a diff_3 with none in-between
    // 7, 10 is a diff_3 with none in-between
    // 4, 7 is a diff_3 with 5, 6 in-between
    // We have:
    // 1, 4, 7, 10;
    // 1, 4, 5, 7, 10;
    // 1, 4, 6, 7, 10;
    // 1, 4, 5, 6, 7, 10
    // So this section has 4 combinations!

    adapters.len()
}

pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_d09() {
        let testdata1 = r#"
            16
            10
            15
            5
            1
            11
            7
            19
            6
            12
            4
            "#;
        let testdata2 = r#"
            28
            33
            18
            42
            31
            14
            46
            20
            48
            47
            24
            23
            49
            45
            19
            38
            39
            11
            1
            32
            25
            35
            8
            17
            7
            9
            4
            2
            34
            10
            3
            "#;
        assert_eq!((7, 5), get_jolt_diffs(testdata1));
        assert_eq!((22, 10), get_jolt_diffs(testdata2));
        //assert_eq!(8, do_part2(testdata1));
        //assert_eq!(19208, do_part2(testdata1));
    }
}
