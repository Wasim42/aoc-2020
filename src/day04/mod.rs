mod input;

use std::collections::HashMap;

fn do_part1(ids: &str) -> i32 {
    count_valid_ids(ids, false)
}

fn do_part2(ids: &str) -> i32 {
    count_valid_ids(ids, true)
}

#[derive(Debug, PartialEq, Eq, Hash)]
enum Fields {
    Byr, // Birth Year
    Iyr, // Issue Year
    Eyr, // Expiry Year
    Hgt, // Height
    Hcl, // Hair Colour
    Ecl, // Eye Colour
    Pid, // Passport ID
    Cid, // Country ID
}

fn valid_number(nums: &str, min: i32, max: i32) -> bool {
    if !nums.is_empty() {
        if let Ok(num) = nums.parse::<i32>() {
            if num >= min && num <= max {
                return true;
            }
        }
    }
    false
}

fn valid_height(nums: &str) -> bool {
    let len = nums.len();

    (&nums[len-2..] == "in" && valid_number(&nums[..len-2], 59, 76)) ||
        (&nums[len-2..] == "cm" && valid_number(&nums[..len-2], 150, 193))
}

fn valid_hair(colour: &str) -> bool {
    colour.len() == 7 && &colour[0..1] == "#" &&
        colour[1..].chars().all(|x| x.is_digit(16))
}

fn valid_eye(colour: &str) -> bool {
    ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&colour)
}

fn valid_passport(ids: &str) -> bool {
    ids.len() == 9 && ids.chars().all(|x| x.is_digit(10))
}

fn count_valid_ids(ids: &str, extra_checks: bool) -> i32 {
    let mut passport: HashMap<Fields, &str> = HashMap::new();

    let mut valid_ids = 0;
    for line in ids.lines() {
        let line = line.trim();
        if line.is_empty() {
            //println!("{:?}", passport);
            passport = HashMap::new();
            continue;
        }
        // Parse this line
        for entry in line.split(' ') {
            let mut fe = entry.split(':');
            let field = fe.next().unwrap();
            let field = match field {
                "byr" => Fields::Byr,
                "iyr" => Fields::Iyr,
                "eyr" => Fields::Eyr,
                "hgt" => Fields::Hgt,
                "hcl" => Fields::Hcl,
                "ecl" => Fields::Ecl,
                "pid" => Fields::Pid,
                "cid" => Fields::Cid,
                _ => panic!("Invalid field {}!", field),
            };
            let nextval = fe.next().unwrap();
            // Check for validity
            if extra_checks {
                // We continue if the field value is invalid
                if (field == Fields::Byr && (nextval.len() == 4)
                    && !valid_number(nextval, 1920, 2002))
                    || (field == Fields::Iyr && (nextval.len() == 4) &&
                        !valid_number(nextval, 2010, 2020))
                    || (field == Fields::Eyr && (nextval.len() == 4) &&
                        !valid_number(nextval, 2020, 2030))
                    || (field == Fields::Hgt && !valid_height(nextval))
                    || (field == Fields::Hcl && !valid_hair(nextval))
                    || (field == Fields::Ecl && !valid_eye(nextval))
                    || (field == Fields::Pid && !valid_passport(nextval))
                {
                    continue;
                }
            }
            passport.insert(field, nextval);
        }
        // End of the line - have we got a valid id yet?
        if passport.len() == 8
            || !passport.contains_key(&Fields::Cid) && passport.len() == 7 {
                valid_ids += 1;
                // Invalidate the passport to avoid double-counting when cid
                // is on a blank line
                passport = HashMap::new();
        }
    }
    valid_ids
}

pub fn part1() -> String {
    format!("{}", do_part1(input::INPUT))
}

pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT))
}


#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT1: &str = r#"
    ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
    byr:1937 iyr:2017 cid:147 hgt:183cm

    iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
    hcl:#cfa07d byr:1929

    hcl:#ae17e1 iyr:2013
    eyr:2024
    ecl:brn pid:760753108 byr:1931
    hgt:179cm

    hcl:#cfa07d eyr:2025 pid:166559648
    iyr:2011 ecl:brn hgt:59in
    "#; 

    #[test]
    fn test_d04_part1() {
        assert_eq!(2, do_part1(TEST_INPUT1));
    }
    const TEST_INPUT2A: &str = r#"
    eyr:1972 cid:100
    hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

    iyr:2019
    hcl:#602927 eyr:1967 hgt:170cm
    ecl:grn pid:012533040 byr:1946

    hcl:dab227 iyr:2012
    ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

    hgt:59cm ecl:zzz
    eyr:2038 hcl:74454a iyr:2023
    pid:3556412378 byr:2007
    "#;

    const TEST_INPUT2B: &str = r#"
    pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
    hcl:#623a2f

    eyr:2029 ecl:blu cid:129 byr:1989
    iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

    hcl:#888785
    hgt:164cm byr:2001 iyr:2015 cid:88
    pid:545766238 ecl:hzl
    eyr:2022

    iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
    "#;

    #[test]
    fn test_d04_part2() {
        assert_eq!(0, do_part2(TEST_INPUT2A));
        assert_eq!(4, do_part2(TEST_INPUT2B));
    }
}
