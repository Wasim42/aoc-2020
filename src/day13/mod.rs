mod input;

fn do_part1(buses: &str) -> i32 {
    let mut arrival = 0;
    let mut timetables = "";
    for line in buses.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        if arrival == 0 {
            arrival = line.parse::<i32>().unwrap();
            continue;
        }
        timetables = line;
    }
    // We have the time we arrive at the bus station, and the timetable.  Now go
    // through all the entries in the timetable, searching for the one closest
    // to our arrival time.
    let mut diff = arrival;
    let mut busid = 0;

    for bus in timetables.split(',') {
        if bus == "x" {
            continue;
        }
        let bus = bus.parse::<i32>().unwrap();

        let depart = (arrival / bus)*bus  + bus;
        if depart - arrival < diff {
            diff = depart - arrival;
            busid = bus;
        }
    }
    busid * diff
}

pub fn part1() -> String {
    format!("{}", do_part1(input::INPUT))
}

fn do_part2(buses: &str) -> i32 {
    buses.len() as i32
}

pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_d11() {
        let testdata1 = r#"
            939
            7,13,x,x,59,x,31,19
            "#;
        assert_eq!(295, do_part1(testdata1));
    }
}
