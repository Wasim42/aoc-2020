mod input;

pub fn part1() -> String {
    let mut answer = 0;
    for line in input::INPUT.lines() {
        // 1-2 p: zhpjph
        // min-max letter: password
        let mut params = line.split(' ');
        let minmax = params.next().unwrap();
        let mut mmparams = minmax.split('-');
        let min = mmparams.next().unwrap().parse::<usize>().unwrap();
        let max = mmparams.next().unwrap().parse::<usize>().unwrap();
        let letter = params.next().unwrap().split(':').next().unwrap();
        let password = params.next().unwrap();

        let v: Vec<_> = password.matches(letter).collect();
        let letter_count = v.len();

        if letter_count >= min && letter_count <= max {
            answer += 1;
        }
    }
    format!("{}", (answer))
}

fn do_part2(s: &str) -> i32 {
    let mut answer = 0;
    for line in s.lines() {
        // 3-5 p: zhpjph
        // one-two letter: password
        let mut params = line.split(' ');
        let onetwo = params.next().unwrap();
        let mut otparams = onetwo.split('-');
        let one = otparams.next().unwrap().parse::<usize>().unwrap();
        let two = otparams.next().unwrap().parse::<usize>().unwrap();
        let letter = params.next().unwrap().split(':').next().unwrap();
        let password = params.next().unwrap();

        let mut found_one = false;
        let mut found_two = false;
        for pos in password.match_indices(letter) {
            if pos.0 == one-1 {
                found_one = true;
            }
            if pos.0 == two-1 {
                found_two = true;
            }
        }
        // Either we find the first one, OR the second one, but not BOTH
        // aka XOR
        if found_one ^ found_two {
            answer += 1;
        }
    }
    answer
}

pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_d2_part2() {
        let ex1=r#"1-3 a: abcde
1-3 b: cdefg
1-3 a: aaa
1-3 a: baa
1-10 a: aaa
10-11 a: aaa
10-11 a: bbbbbbbbbaa
10-11 a: bbbbbbbbbab
2-9 c: ccccccccc"#;
        assert_eq!(4, do_part2(ex1));
    }
}
