mod input;

fn to_numbers(numbers: &str) -> Vec<i64> {
    let mut nums = Vec::new();
    for line in numbers.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        nums.push(line.parse::<i64>().unwrap());
    }
    nums
}

fn find_invalid(xmas: &str, preamble: usize) -> i64 {

    let mut invalid = 0;
    let numbers = to_numbers(xmas);
    for pos in preamble..numbers.len() {
        let check = numbers[pos];
        // Now check all combinations of the previous 2 numbers and see of they
        // add up.  If not, we have found our number

        let mut found = false;
        for cpos in pos-preamble..pos {
            for icpos in cpos..pos {
                if numbers[cpos] + numbers[icpos] == check {
                    found = true;
                    break;
                }
            }
            if found {
                break;
            }
        }
        if !found {
            invalid = check;
            break;
        }
    }
    invalid
}

pub fn part1() -> String {
    format!("{}", find_invalid(input::INPUT, 25))
}

fn do_part2(xmas: &str, preamble: usize) -> i64 {
    let invalid = find_invalid(xmas, preamble);

    let numbers = to_numbers(xmas);

    let mut low_pos = 0;
    let mut high_pos = 0;
    // Sum all combinations of numbers until we total invalid
    for pos in 0..numbers.len() {
        let mut sum = numbers[pos];
        for (ipos, number) in numbers[pos+1..numbers.len()].iter().enumerate() {
            sum += number;
            if sum == invalid {
                low_pos = pos;
                high_pos = ipos+pos;
                break;
            }
        }
        if low_pos != high_pos {
            break;
        }
    }

    // We need to sum the lowest and highest value of numbers that total invalid
    let mut low_val = numbers[low_pos];
    let mut high_val = numbers[high_pos];

    for &val in numbers[low_pos..high_pos+1].iter() {
        if val < low_val {
            low_val = val;
        } else if val > high_val {
            high_val = val;
        }
    }
    low_val + high_val
}

pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT, 25))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_d09() {
        let testdata1 = r#"
            35
            20
            15
            25
            47
            40
            62
            55
            65
            95
            102
            117
            150
            182
            127
            219
            299
            277
            309
            576
            "#;
        assert_eq!(127, find_invalid(testdata1, 5));
        assert_eq!(62, do_part2(testdata1, 5));
    }
}
