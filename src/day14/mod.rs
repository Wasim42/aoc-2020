mod input;

use std::collections::HashMap;

fn parse_mask(mask: &str, value: u8) -> u64 {
    // All we have to do is transform the mask into two numbers, one using the
    // 1's, and the other using the 0's
    // For the 1's number, transform all the non-1's into zeroes.
    // For the 0's number, transform all the non-0's into ones.

    let mut power_2 = 1;

    let mut number = 0;

    for bit in mask.chars().rev() {
        if value == 0 && bit != '0' {
            // We are building the zero's value
            number += power_2;
        }
        if value == 1 && bit == '1' {
            // We are building the one's value
            number += power_2;
        }
        power_2 *= 2;
    }

    number
}

fn do_part1(instructions: &str) -> u64 {
    let mut memory = HashMap::new();


    // mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
    // mem[8] = 11
    // mem[7] = 101
    // mem[8] = 0
    // We can then AND the memory location with 0's number, and OR it with the
    // 1's number.

    // zeroes and ones are manifestations of the current mask in effect
    let mut zeroes = 0;
    let mut ones = 0;

    for line in instructions.lines() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        //println!("{}", line);
        let mut cmdval = line.split(" = ");
        let cmd = cmdval.next().unwrap();
        let val = cmdval.next().unwrap();
        if cmd == "mask" {
            ones = parse_mask(val, 1);
            zeroes = parse_mask(val, 0);
            //println!("{} {}", ones, zeroes);
            continue
        }

        // We are dealing with a memory location
        let number = val.parse::<u64>().unwrap();
        let locket = cmd.split("mem[").nth(1).unwrap();
        let location = locket.split(']').next().unwrap();
        let location = location.parse::<usize>().unwrap();

        let mut entry = number;
        entry &= zeroes;
        entry |= ones;

        memory.insert(location, entry);

        //println!(" => mem[{}] = {}", location, entry);

    }
    memory.values().sum()
}

pub fn part1() -> String {
    format!("{}", do_part1(input::INPUT))
}

fn do_part2(cells: &str) -> i32 {
    cells.len() as i32
}

pub fn part2() -> String {
    format!("{}", do_part2(input::INPUT))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_all() {
        let testdata1 = r#"
            mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
            mem[8] = 11
            mem[7] = 101
            mem[8] = 0
            "#;
        assert_eq!(165, do_part1(testdata1));
    }
}
