mod algorithms;
mod structs;

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day19;

use std::env;


macro_rules! add_day {
    ($day:ident) => {
        [$day::part1, $day::part2]
    }
}

macro_rules! do_part {
    ($days:expr, $day:expr, $part:expr) => {
        println!("Day {} part {}: {}", $day, $part, $days[$day-1][$part-1]());
    }
}

fn help(name: &str) {
    println!("{} <day> <part>", name);
    println!("\n  Running just {} will run all the days and parts", name);
}

fn main() {

    let args: Vec<_> = env::args().collect();

    if args.len() > 3 {
        help(&args[0]);
        return;
    }

    // Fill in the days we know about
    let days = vec![
        add_day!(day01),
        add_day!(day02),
        add_day!(day03),
        add_day!(day04),
        add_day!(day05),
        add_day!(day06),
        add_day!(day07),
        add_day!(day08),
        add_day!(day09),
        add_day!(day10),
        add_day!(day11),
        add_day!(day12),
        add_day!(day13),
        add_day!(day14),
        add_day!(day15),
        add_day!(day19),
        add_day!(day19),
        add_day!(day19),
        add_day!(day19),
    ];

    // How this will work:
    // Running just aoc2020 will run through all the days
    // Running "aoc2020 1" will run both parts of day 1
    // Running "aoc2020 1 1" will run just day 1 part 1
    // By default we run all the days
    if args.len() == 1 {
        for day in 0..days.len() {
            do_part!(days, day+1, 1);
            do_part!(days, day+1, 2);
        }
        return;
    }

    let day: usize;
    if let Ok(daynum) = args[1].parse::<usize>() {
        if daynum > 0 && daynum <= days.len() {
            day = daynum;
        } else {
            println!("Invalid day {}", daynum);
            println!("Days between 1 and {} only", days.len());
            return;
        }
    } else {
        println!("Can't parse {} as a day", args[1]);
        help(&args[0]);
        return;
    }
    // We have our day, do we have any parts?
    if args.len() == 2 {
        // Only the day specified, do both parts
        do_part!(days, day, 1);
        do_part!(days, day, 2);
        return;
    }
    if args[2] == "1" {
        do_part!(days, day, 1);
    } else if args[2] == "2" {
        do_part!(days, day, 2);
    } else {
        println!("Only parts 1 and 2 are valid");
        help(&args[0]);
    }
}
