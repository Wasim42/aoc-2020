// Day 1 - and so we begin

use std::collections::HashSet;

mod input;


fn input_to_i32() -> HashSet<i32> {
    input::INPUT.lines().map(|x| x.parse::<i32>().unwrap()).collect()
}

pub fn do_part1(nums: HashSet<i32>) -> i32 {
    for i in &nums {
        if nums.contains(&(2020 - i)) {
            return i*(2020-i);
        }
    }
    0
}

pub fn part1() -> String {
    let nums = input_to_i32();
    format!("{}", do_part1(nums))
}

pub fn do_part2(nums: HashSet<i32>) -> i32 {
    // Outer loop
    for i in &nums {
        for j in &nums {
            if i + j >= 2020 {
                continue;
            }
            if i == j {
                continue;
            }
            let remainder = 2020 - (i+j);
            if nums.contains(&remainder) {
                return i*j*remainder;
            }
        }
    }
    0
}

pub fn part2() -> String {
    let nums = input_to_i32();
    format!("{}", do_part2(nums))
}
